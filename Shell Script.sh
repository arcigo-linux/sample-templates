#!/bin/bash

# Title	   	   : Simple Bash Template
# Purpose	   : Simplify the scripting the process
# Author	   : NAME
# Usage		   : ./simple-template.sh
# Version	   : 0.2
# Created Date : 
# Modified Date: 
# Info		   : Use this as starting point to automate the process

# External variables (User may allowed to change)
# These can be specific to service or software that you're trying to setup

# Internal variables (User may not allowed to change)
PROGRAM="program-name"
DEPENDENCIES=" " # List the dependency packages
PKGMGR=" " # Package manager with update and install command

# START #
# Get Colors
function _red_text(){
	QUERY="$*"
	echo -e "\e[31m${QUERY}\e[0m"
}

function _green_text(){
	QUERY="$*"
	echo -e "\e[32m${QUERY}\e[0m"
}

function _yellow_text(){
	QUERY="$*"
	echo -e "\e[33m${QUERY}\e[0m"
}

# It is the main function, calls other functions
function main(){
	verify_root
	_green_text ${0}: Setting up ${PROGRAM}...
	install_dependencies
	do_stuff
	exit 0
}

# If there is any dependencies, install them !
function install_dependencies(){
	_green_text [+] Installing dependencies...
	$PKGMGR $DEPENDENCIES 2> /dev/null || panic
}

# Make multiple versions of this function that each
# try to accomplish one specific task
function do_stuff(){
	_green_text [*] Do some stuff...
}

# Panic if something goes wrong; we can use this is
# in script with the || syntax, so if one command fails
# we can panic the user!
function panic(){
	_red_text [-] Fatal error, aborting the process...
	exit -1
}

# Make sure that the user running this as root
function verify_root(){
	if [ "$UID" != 0 ]; then
		_red_text [$0]: You must be root to run this script !
		exit -1
	fi
}


# Calling main function, passing in all passed arguments to every function
main "$@"

# END #
